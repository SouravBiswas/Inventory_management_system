<?php include '../classes/Brand.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$brand = new Brand();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $brandName = $_POST['brandName'];
    $brandStatus = $_POST['brandStatus'];
    $insertBrand = $brand->brandInsert($brandName,$brandStatus);
}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Brand</h2>
            <div class="block copyblock">
                <?php
                if(isset($insertBrand)){
                    echo $insertBrand;
                }
                ?>
                <form action="" method="post">
                    <table class="form" style="height: 150px">
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="brandName" placeholder="Enter Brand Name..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 318px" class="form-control" id="brandStatus" name="brandStatus">
                                    <option value="">~~Select Brand Status~~</option>
                                    <option value="1">Available</option>
                                    <option value="2">Not Available</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>