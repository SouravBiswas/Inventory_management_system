<?php include '../classes/Brand.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
if(!isset($_GET['brandid']) || $_GET['brandid'] == NULL){
    echo "<script>window.location = 'brandlist.php'</script>";
}else{

    $id = $_GET['brandid'];
}
$brand = new Brand();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //$id=$_GET['catid'];
    $brandName = $_POST['brandName'];
    $brandStatus = $_POST['brandStatus'];
    $updateBrand = $brand->brandUpdate($brandName,$brandStatus,$id);

}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Brand</h2>
            <div class="block copyblock">
                <?php
                if(isset($updateBrand)){
                    echo $updateBrand;
                }
                ?>
      <?php
         $getBrand = $brand->getbrandById($id);
         if($getBrand){

         while ($result = mysqli_fetch_assoc($getBrand)){

        ?>

        <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="brandName" value="<?php echo $result['brandName']?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 318px" class="form-control" id="brandStatus" name="brandStatus">
                                    <option value="">~~Select Brand Status~~</option>
             <?php if($result['brand_active'] == 1){?>
                                    <option selected="selected" value="1">Available</option>
                                    <option value="2">Not Available</option>
             <?php } else {?>
                                    <option selected="selected" value="2">Not Available</option>
                                    <option value="1">Available</option>
             <?php }?>
                                                    </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
             <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>