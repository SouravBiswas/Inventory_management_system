﻿<?php include '../classes/Brand.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php
   $brandList = new Brand();
   if(isset($_GET['delbrand'])){
     $id = $_GET['delbrand'];
     $deleteBrand = $brandList->delBrandById($id);
}
   $getAllBrand = $brandList->getAllBrand();
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Brand List</h2>
                <div class="block">
                    <?php
                    if(isset($deleteBrand)){
                        echo $deleteBrand;
                    }
                    ?>
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Brand Name</th>
                            <th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    if($getAllBrand){
                        $i=0;
                        while ($result = mysqli_fetch_assoc($getAllBrand)){
                            $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo $i?></td>
							<td><?php echo $result['brandName']?></td>
                                <?php
                                if(($result['brand_active']==1)){
                                    echo "<td class='label label-success'>Available</td>";
                                }else{
                                    echo "<td class='label label-danger'>Not Available</td>";
                                }

                                ?>

							<td><a href="brandedit.php?brandid=<?php echo $result['brandId'] ?>">Edit</a> ||
                                <a onclick="return confirm('Are you sure to delete!!')" href="?delbrand=<?php echo $result['brandId'];  ?>">Delete</a></td>
						</tr>
                    <?php } }?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

