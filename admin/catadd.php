﻿<?php include '../classes/Category.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$category = new Category();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $catName = $_POST['catName'];
    $catStatus = $_POST['catStatus'];
    $insertCat = $category->catInsert($catName,$catStatus);
}

?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock">
                   <?php
                   if(isset($insertCat)){
                       echo $insertCat;
                   }
                   ?>
                 <form action="" method="post">
                    <table class="form" style="height: 150px">
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="catName" placeholder="Enter Category Name..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 318px" class="form-control" id="brandStatus" name="catStatus">
                                    <option value="">~~Select Category Status~~</option>
                                    <option value="1">Available</option>
                                    <option value="2">Not Available</option>
                                </select>
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
      </script>
<?php include 'inc/footer.php';?>