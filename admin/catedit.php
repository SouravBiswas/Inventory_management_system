<?php include '../classes/Category.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
if(!isset($_GET['catid']) || $_GET['catid'] == NULL){
    echo "<script>window.location = 'catlist.php'</script>";
}else{

    $id = $_GET['catid'];
}
$category = new Category();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //$id=$_GET['catid'];
    $catName = $_POST['catName'];
    $catStatus = $_POST['catStatus'];
    $updateCat = $category->catUpdate($catName,$catStatus,$id);

}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Category</h2>
            <div class="block copyblock">
                <?php
                if(isset($updateCat)){
                    echo $updateCat;
                }
                ?>
      <?php
         $getCat = $category->getcatById($id);
         if($getCat){

         while ($result = mysqli_fetch_assoc($getCat)){

        ?>

        <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="catName" value="<?php echo $result['catName']?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 318px" class="form-control" id="brandStatus" name="catStatus">
                                    <option value="">~~Select Category Status~~</option>
                                    <?php if($result['cat_active'] == 1){?>
                                        <option selected="selected" value="1">Available</option>
                                        <option value="2">Not Available</option>
                                    <?php } else {?>
                                        <option selected="selected" value="2">Not Available</option>
                                        <option value="1">Available</option>
                                    <?php }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
             <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>