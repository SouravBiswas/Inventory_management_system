﻿<?php include '../classes/Category.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php
   $catList = new Category();
   if(isset($_GET['delcat'])){
     $id = $_GET['delcat'];
     $deleteCategory = $catList->delCatById($id);
}
   $getAllCat = $catList->getAllCat();
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Category List</h2>
                <div class="block">
                    <?php
                    if(isset($deleteCategory)){
                        echo $deleteCategory;
                    }
                    ?>
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Category Name</th>
                            <th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    if($getAllCat){
                        $i=0;
                        while ($result = mysqli_fetch_assoc($getAllCat)){
                            $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo $i?></td>
							<td><?php echo $result['catName']?></td>

                            <?php
                            if(($result['cat_active']==1)){
                                echo "<td class='label label-success'>Available</td>";
                            }else{
                                echo "<td class='label label-danger'>Not Available</td>";
                            }

                            ?>

                            <td><a href="catedit.php?catid=<?php echo $result['catId'] ?>">Edit</a> ||
                                <a onclick="return confirm('Are you sure to delete!!')" href="?delcat=<?php echo $result['catId'];  ?>">Delete</a></td>
						</tr>
                    <?php } }?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

