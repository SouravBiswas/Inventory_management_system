<?php include '../classes/Category.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Customer.php';?>
<?php
if(!isset($_GET['custId']) || $_GET['custId'] == NULL){
    echo "<script>window.location = 'report.php'</script>";
}else{

    $id = $_GET['custId'];
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    echo "<script>window.location = 'report.php'</script>";
}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Customer Details</h2>
            <div class="block copyblock">
                <?php
                if(isset($updateCat)){
                    echo $updateCat;
                }
                ?>
                <?php
                $customer = new Customer();
                $getCustomer = $customer->getCustomerData($id) ;
                if($getCustomer){

                    while ($result = mysqli_fetch_assoc($getCustomer)){

                        ?>

                        <form action="" method="post">
                            <table class="form">
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['name']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['address']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['city']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Zip Code</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['zip']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['phone']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result['email']?>" class="medium" />
                                    </td>
                                </tr>
                                <tr style="">
                                    <td >
                                        <input type="submit" name="submit" Value="OK" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>