<?php include '../classes/Customer.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$cmr = new Customer();
$adminId = Session::get('adminId');
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $adminUpdate = $cmr->adminUpdate($_POST,$adminId);
}

?>

<center>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Profile Information</h2>
            <div class="block copyblock">
                <?php
                if(isset($adminUpdate)){
                    echo $adminUpdate;
                }
                ?>
                <?php
                $id = Session::get('adminId');
                $getData = $cmr->getAdminInfo($id);
                if ($getData){
                while ($result = mysqli_fetch_assoc($getData)){

                ?>

                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td style="font-size: 20px">
                                <level>Name :</level>
                                <input style="margin-left: 66px" type="text" name="adminName" value="<?php echo $result['adminName']?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 20px">
                                <level>User Name :</level>
                                <input style="margin-left: 20px" type="text" name="adminUser" value="<?php echo $result['adminUser']?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 20px">
                                <level>Email :</level>
                                <input style="margin-left: 69px" type="text" name="adminEmail" value="<?php echo $result['adminEmail']?>" class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: center;padding-top: 50px">
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <?php }}?>
        </div>
    </div>
</center>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>