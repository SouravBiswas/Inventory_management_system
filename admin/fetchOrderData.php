<?php 	

require_once 'core.php';

$orderId = $_POST['orderId'];

$valid = array('order' => array(), 'order_item' => array());

$sql = "SELECT tbl_order.order_id, tbl_order.order_date, tbl_order.client_name, tbl_order.client_contact, tbl_order.sub_total, tbl_order.vat, tbl_order.total_amount, tbl_order.discount, tbl_order.grand_total, tbl_order.paid, tbl_order.due, tbl_order.payment_type, tbl_order.payment_status FROM tbl_order 	
	WHERE tbl_order.order_id = {$orderId}";

$result = $connect->query($sql);
$data = $result->fetch_row();
$valid['order'] = $data;


$connect->close();

echo json_encode($valid);