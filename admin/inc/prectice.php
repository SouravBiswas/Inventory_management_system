<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<form >
    <input type="text" placeholder="Enter your email address ..." id="email" name="emailAddress"   />
    <button class="btn validate" type="submit" disabled >Join</button>
</form>

<script>
    jQuery(document).ready(function($) {
        //  var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        <!-- another regex filter you can use -->
        var testEmail =    /^[ ]*([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})[ ]*$/i;
        jQuery('input#email').bind('input propertychange', function() {
            if (testEmail.test(jQuery(this).val()))
            {
                jQuery(this).css({ 'border':'1px solid green'});
                jQuery('button.validate').prop("disabled",false);
            } else
            {
                jQuery(this).css({ 'border':'1px solid red'});
                jQuery('button.validate').prop("disabled",true);
            }
        });
    });
</script>
</body>
</html>