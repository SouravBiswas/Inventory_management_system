<div class="grid_2">
    <div class="box sidemenu">
        <div class="block" id="section-menu">
            <ul class="section menu">
               <li><a class="menuitem" style="color:#ac2925;font-weight: bold;font-size: 20px">Menu Option</a>

                </li>
				<li><a class="menuitem" style="color: #2b669a;font-weight: bold;font-size: 16px">Category Option</a>
                    <ul class="submenu">
                        <li><a href="catadd.php">Add Category</a> </li>
                        <li><a href="catlist.php">Category List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem" style="color: #2b669a;font-weight: bold;font-size: 16px">Brand Option</a>
                    <ul class="submenu">
                        <li><a href="brandadd.php">Add Brand</a> </li>
                        <li><a href="brandlist.php">Brand List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem" style="color: #2b669a;font-weight: bold;font-size: 16px">Product Option</a>
                    <ul class="submenu">
                        <li><a href="productadd.php">Add Product</a> </li>
                        <li><a href="productlist.php">Product List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem" style="color: #2b669a;font-weight: bold;font-size: 16px">Supplier Option</a>
                    <ul class="submenu">
                        <li><a href="supplieradd.php">Add Supplier</a> </li>
                        <li><a href="supplierlist.php">Manage Supplier</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem" style="color: #2b669a;font-weight: bold;font-size: 16px">Orders</a>
                    <ul class="submenu">
                        <li><a href="addorder.php">Add Orders</a> </li>
                        <li><a href="orderlist.php">Manage Orders</a> </li>
                    </ul>
                </li>


            </ul>
        </div>
    </div>
</div>