﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Brand.php';?>
<?php include '../classes/Category.php';?>
<?php include '../classes/Product.php';?>
<?php include '../classes/Customer.php';?>
<?php
$brand = new Brand();
$cat = new Category();
$pd = new Product();
$customer = new Customer();

$allSupplier = $brand->getAllSupplier();
$allSupplier = mysqli_num_rows($allSupplier);

$allOrder = $customer->getAllOrderCount();
$getAllOrder = mysqli_num_rows($allOrder);

$getAllBrand = $brand->getAllBrand();
$allBrandCount = mysqli_num_rows($getAllBrand);

$getAllCategory = $cat->getAllCat();
$allCatCount = mysqli_num_rows($getAllCategory);

$getAllProduct = $pd->getAllProductCount();
$allProductCount = mysqli_num_rows($getAllProduct);

$stockChecker = $customer->stockChecker();
//$stockChecker = mysqli_num_rows($getStock);

$getRevenue = $customer->calculateRevenue();
if($getRevenue){
    $totalRevenue = "";
    while ($revenue = mysqli_fetch_assoc($getRevenue)){
        $totalRevenue += $revenue['paid'];
    }
}

?>

    <div class="grid_10">
        <div class="box round first grid">
            <h2> Dashbord</h2><br>
            <div class="row">
                    <div class="col-lg-3 col-md-6" style="">
                        <div class="panel panel-primary" style="">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allProductCount?></div>
                                        <div>Total Product</div>
                                    </div>
                                </div>
                            </div>
                            <a href="productlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo  $getAllOrder?></div>
                                        <div>Total Order</div>
                                    </div>
                                </div>
                            </div>
                            <a href="orderlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $stockChecker;?>

                                            </div>
                                        <div>Low Stock</div>
                                    </div>
                                </div>
                            </div>
                            <a href="productlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allCatCount?></div>
                                        <div>Total Category</div>
                                    </div>
                                </div>
                            </div>
                            <a href="catlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allBrandCount?></div>
                                        <div>Total Brand</div>
                                    </div>
                                </div>
                            </div>
                            <a href="brandlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php if($totalRevenue){
                                            echo $totalRevenue;
                                            }else{
                                            echo '0';
                                            }?></div>
                                        <div>Total Revenue</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class=""></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php if($allSupplier){
                                            echo $allSupplier;
                                        }else{
                                            echo '0';
                                        }?></div>
                                    <div>Total Suppliers</div>
                                </div>
                            </div>
                        </div>
                        <a href="supplierlist.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                </div>
                <!-- /.row -->

        <div class="box round first grid " style="margin-top: 0px;height: 200px">
            <h2 class="" style="text-align: center;font-size: 20px"><i class="glyphicon glyphicon-calendar" ></i>Calender</h2><br>
            <div class="col-lg-3 col-md-10" style="margin-left: 400px;width: 400px;">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="" ></i>
                            </div>
                            <div class="col-xs-6 text-center">
                                <div class="huge" style="font-size: 60px" ><?php echo date('d'); ?></div>
                                <div style="font-size: 25px;color: #2b542c">Today</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left"><?php echo date('l') .' '.date('d').', '.date('Y'); ?></span>
                            <span class="pull-right"><i class=""></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php include 'inc/footer.php';?>