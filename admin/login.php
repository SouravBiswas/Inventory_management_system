<?php include '../classes/Adminlogin.php'?>
<?php

$al = new Adminlogin();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $adminUser = $_POST['adminUser'];
    $adminPass = md5($_POST['adminPass']);
    $loginChk = $al->adminLogin($adminUser,$adminPass);
}

?>

<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>Admin Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
    <link rel="stylesheet" href="css/validation.css">
    <script src="js/jquery.min.js"></script>
    <script>
        $(function () {
            $("#username_msg").hide();
            $("#capital").hide();
            $("#number").hide();
            $("#length").hide();

            var error_username = false;
            var error_password = false;

            $("#username").focusin(function () {
                check_username();
            });

            $("#password").focusin(function () {
                check_password();
            });

            function check_username() {

                $('#username').keyup(function () {
                    var username_length = $("#username").val().length;
                    if(username_length < 5 || username_length > 20){
                        $("#username_msg").html(" Username Should be between 5-20 characters");
                        $("#username_msg").addClass('alert');
                        $("#username_msg").show();
                        error_username = true;

                    }else {
                        $("#username_msg").hide();
                        error_username = false;
                    }
                })

            }
            function check_password() {

                $('#password').keyup(function () {
                    var password_length = $("#password").val().length;
                    if(password_length < 8 ){
                        $("#pswd_info").html(" Password more then 8 characters");
                        $("#pswd_info").addClass('alert');
                        $("#pswd_info").show();
                        error_password = true;

                    }else {
                        $("#pswd_info").hide();
                        error_password = false;
                    }
                })

            }
            $('#regform').submit(function () {

                check_username();
                check_password();
                if(error_username == false && error_password == false){
                    return true;
                }else {
                    return false;
                }
            });
        });
    </script>
    <style>
        .alert{
            color: red;

        }
        #pswd_info{
            font-size: 16px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
	<section id="content" style="width: 500px">
		<form action="" method="post" id="regform">
			<h1>Admin Login</h1>
            <span style="color: red;font-size: 17px">
                <?php
                if(isset($loginChk)){
                    echo $loginChk;
                }

                ?>
            </span>
			<div>
				<input type="text" placeholder="Username" id="username" required=""  name="adminUser"/>
                <div id="username_msg">

                </div>

			</div>
			<div>
				<input type="password" placeholder="Password" id="password" required=""  name="adminPass"/>
                <div id="pswd_info">

                </div>
			</div>
            <div id="">
                <ul>
                    <li id="capital" class="invalid" ></li>
                    <li id="number" class="invalid"></li>
                    <li id="length" class="invalid"></strong></li>
                </ul>
            </div>
			<div>
				<input type="submit" value="Log in" />
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">Inventory Management System</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>

</html>