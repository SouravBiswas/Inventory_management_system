<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php';?>
<?php include '../classes/Customer.php';?>
<?php
$pd = new Product();
//$insertProduct = '';
//if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
  // $insertProduct = $pd->orderInsert($_POST);
   //var_dump($insertProduct);
   //die();
//}

if(!isset($_GET['orderid']) || $_GET['orderid'] == NULL){
    echo "<script>window.location = 'orderlist.php'</script>";
}else{

    $orderid = $_GET['orderid'];

}

$customer = new Customer();

?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Order</h2>
    <div class="block">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-plus"></i>Update Order
            </div>
            <br>
            <div  class="success-messages"></div>
            <?php
            $allOrderData = $customer->allOrderDataById($orderid);
            if($allOrderData){
            while ($value = mysqli_fetch_assoc($allOrderData)){

            ?>
        <form class="form-horizontal" method="POST" action="editOrder.php" id="editOrderForm">

            <div class="form-group">
                <label for="orderDate" class="col-sm-2 control-label">Order Date</label>
                <div class="col-sm-10">
                    <input style="width: 800px" type="text" class="form-control" id="orderDate" value="<?php echo $value['order_date']?>" name="orderDate" autocomplete="off" />
                </div>
            </div> <!--/form-group-->
            <div class="form-group">
                <label for="clientName" class="col-sm-2 control-label">Client Name</label>
                <div class="col-sm-10">
                    <input style="width: 800px" type="text" class="form-control" id="clientName" value="<?php echo $value['client_name']?>" name="clientName" placeholder="Client Name" autocomplete="off" />
                </div>
            </div> <!--/form-group-->
            <div class="form-group">
                <label for="clientContact" class="col-sm-2 control-label">Client Contact</label>
                <div class="col-sm-10">
                    <input style="width: 800px" type="text" class="form-control" id="clientContact" value="<?php echo $value['client_contact']?>" name="clientContact" placeholder="Contact Number" autocomplete="off" />
                </div>
            </div> <!--/form-group-->

            <table class="table" id="productTable">
                <thead>
                <tr>
                    <th style="width:40%;">Product</th>
                    <th style="width:20%;">Rate</th>
                    <th style="width:15%;">Quantity</th>
                    <th style="width:15%;">Total</th>
                    <th style="width:10%;"></th>
                </tr>
                </thead>
                <tbody>
                <?php

                // $orderItemData = $orderItemResult->fetch_all();

                $orderItem = $customer->orderItem($orderid);

                // print_r($orderItemData);
                $arrayNumber = 0;
                // for($x = 1; $x <= count($orderItemData); $x++) {
                $x = 1;
                while($orderItemData = mysqli_fetch_assoc($orderItem)) {
                    // print_r($orderItemData); ?>

                    <tr id="row<?php echo $x; ?>" class="<?php echo $arrayNumber; ?>">
                    <td style="margin-left:20px;">
                        <div class="form-group">

                            <select class="form-control" name="productName[]" id="productName<?php echo $x; ?>" onchange="getProductData(<?php echo $x; ?>)" >
                                <option value="">~~SELECT~~</option>


                                <?php
                                $productData = $pd->productDataByStatus();

                                while($row = mysqli_fetch_assoc($productData)) {

                                  //  $selected = "";
                                    if($row['productId'] == $orderItemData['product_id']) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }

                                    echo "<option value='".$row['productId']."' id='changeProduct".$row['productId']."' ".$selected.">".$row['productName']."</option>";
                                } // /while

                                ?>
                            </select>
                        </div>
                    </td>
                    <td style="padding-left:20px;">
                        <input type="text" value="<?php echo $orderItemData['rate']; ?>" name="rate[]" id="rate<?php echo $x; ?>" autocomplete="off" disabled="true" class="form-control" />
                        <input type="hidden" value="<?php echo $orderItemData['rate']; ?>" name="rateValue[]" id="rateValue<?php echo $x; ?>" autocomplete="off" class="form-control" />
                    </td>
                    <td style="padding-left:20px;">
                        <div class="form-group">
                            <input type="number" value="<?php echo $orderItemData['quantity']; ?>" name="quantity[]" id="quantity<?php echo $x; ?>" onchange="getTotal(<?php echo $x ?>)" autocomplete="off" class="form-control" min="1" />
                        </div>
                    </td>
                    <td style="padding-left:20px;">
                        <input type="text" value="<?php echo $orderItemData['total']; ?>" name="total[]" id="total<?php echo $x; ?>" autocomplete="off" class="form-control" disabled="true" />
                        <input type="hidden" value="<?php echo $orderItemData['total']; ?>" name="totalValue[]" id="totalValue<?php echo $x; ?>" autocomplete="off" class="form-control" />
                    </td>
                    <td>

                        <button style="" class="btn btn-default removeProductRowBtn" type="button" id="removeProductRowBtn" onclick="removeProductRow(<?php echo $x; ?>)"><i class="glyphicon glyphicon-trash"></i></button>
                    </td>
                </tr>
                    <?php
                    $arrayNumber++;
                    $x++;
                } // /for
                ?>
                </tbody>
            </table>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subTotal" class="col-sm-3 control-label">Sub Amount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="subTotal" value="<?php echo $value['sub_total']?>" name="subTotal" disabled="true" />
                        <input type="hidden" class="form-control" id="subTotalValue" value="<?php echo $value['sub_total']?>" name="subTotalValue" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="vat" class="col-sm-3 control-label">VAT 13%</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="vat" name="vat" value="<?php echo $value['vat']?>" disabled="true" />
                        <input type="hidden" class="form-control" id="vatValue" value="<?php echo $value['vat']?>" name="vatValue" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="totalAmount" class="col-sm-3 control-label">Total Amount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="totalAmount" value="<?php echo $value['total_amount']?>" name="totalAmount" disabled="true"/>
                        <input type="hidden" class="form-control" id="totalAmountValue" value="<?php echo $value['total_amount']?>" name="totalAmountValue" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="discount" class="col-sm-3 control-label">Discount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="discount" name="discount" value="<?php echo $value['discount']?>" onkeyup="discountFunc()" autocomplete="off" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="grandTotal" class="col-sm-3 control-label">Grand Total</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="grandTotal" value="<?php echo $value['grand_total']?>" name="grandTotal" disabled="true" />
                        <input type="hidden" class="form-control" id="grandTotalValue" value="<?php echo $value['grand_total']?>" name="grandTotalValue" />
                    </div>
                </div> <!--/form-group-->
            </div> <!--/col-md-6-->

            <div class="col-md-6">
                <div class="form-group">
                    <label for="paid" class="col-sm-3 control-label">Paid Amount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="paid" value="<?php echo $value['paid']?>" name="paid" autocomplete="off" onkeyup="paidAmount()" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="due" class="col-sm-3 control-label">Due Amount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="due" value="<?php echo $value['due']?>" name="due" disabled="true" />
                        <input type="hidden" class="form-control" id="dueValue" value="<?php echo $value['due']?>" name="dueValue" />
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="clientContact" class="col-sm-3 control-label">Payment Type</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="paymentType" id="paymentType">

                            <option value="">~~SELECT~~</option>
                            <option value="1" <?php if($value['payment_type'] == 1) {
                                echo "selected";
                            } ?> >Cheque</option>
                            <option value="2" <?php if($value['payment_type'] == 2) {
                                echo "selected";
                            } ?>  >Cash</option>
                            <option value="3" <?php if($value['payment_type'] == 3) {
                                echo "selected";
                            } ?> >Credit Card</option>

                        </select>
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="clientContact" class="col-sm-3 control-label">Payment Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="paymentStatus" id="paymentStatus">

                            <option value="">~~SELECT~~</option>
                            <option value="1" <?php if($value['payment_status'] == 1) {
                                echo "selected";
                            } ?>  >Full Payment</option>
                            <option value="2" <?php if($value['payment_status'] == 2) {
                                echo "selected";
                            } ?> >Advance Payment</option>
                            <option value="3" <?php if($value['payment_status'] == 3) {
                                echo "selected";
                            } ?> >No Payment</option>


                        </select>
                    </div>
                </div> <!--/form-group-->
            </div> <!--/col-md-6-->


            <div class="form-group submitButtonFooter">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-default" onclick="addRow()" id="addRowBtn" data-loading-text="Loading..."> <i class=""></i> Add Row </button>

                    <input type="hidden" name="orderId" id="orderId" value="<?php echo $_GET['orderid']; ?>" />

                    <button type="submit" name="submit" id="createOrderBtn" data-loading-text="Loading..." class="btn btn-success"><i class=""></i> Save Changes</button>

                   </div>
            </div>


        </form>
            <?php } }?>
        </div>
    </div>
    </div>
</div>


    <script>
        $( function() {
            $( "#orderDate" ).datepicker();
        } );
    </script>

    <script>
        $(document).ready(function () {

            // create order form function
            $("#editOrderForm").unbind('submit').bind('submit', function() {

                var form = $(this);

                $('.form-group').removeClass('has-error').removeClass('has-success');
                $('.text-danger').remove();

                var orderDate = $("#orderDate").val();
                var clientName = $("#clientName").val();
                var clientContact = $("#clientContact").val();
                var paid = $("#paid").val();
                var discount = $("#discount").val();
                var paymentType = $("#paymentType").val();
                var paymentStatus = $("#paymentStatus").val();

                // form validation
                if(orderDate == "") {
                    $("#orderDate").after('<p class="text-danger"> The Order Date field is required </p>');
                    $('#orderDate').closest('.form-group').addClass('has-error');
                } else {
                    $('#orderDate').closest('.form-group').addClass('has-success');
                } // /else

                if(clientName == "") {
                    $("#clientName").after('<p class="text-danger"> The Client Name field is required </p>');
                    $('#clientName').closest('.form-group').addClass('has-error');
                } else {
                    $('#clientName').closest('.form-group').addClass('has-success');
                } // /else

                if(clientContact == "") {
                    $("#clientContact").after('<p class="text-danger"> The Contact field is required </p>');
                    $('#clientContact').closest('.form-group').addClass('has-error');
                } else {
                    $('#clientContact').closest('.form-group').addClass('has-success');
                } // /else

                if(paid == "") {
                    $("#paid").after('<p class="text-danger"> The Paid field is required </p>');
                    $('#paid').closest('.form-group').addClass('has-error');
                } else {
                    $('#paid').closest('.form-group').addClass('has-success');
                } // /else

                if(discount == "") {
                    $("#discount").after('<p class="text-danger"> The Discount field is required </p>');
                    $('#discount').closest('.form-group').addClass('has-error');
                } else {
                    $('#discount').closest('.form-group').addClass('has-success');
                } // /else

                if(paymentType == "") {
                    $("#paymentType").after('<p class="text-danger"> The Payment Type field is required </p>');
                    $('#paymentType').closest('.form-group').addClass('has-error');
                } else {
                    $('#paymentType').closest('.form-group').addClass('has-success');
                } // /else

                if(paymentStatus == "") {
                    $("#paymentStatus").after('<p class="text-danger"> The Payment Status field is required </p>');
                    $('#paymentStatus').closest('.form-group').addClass('has-error');
                } else {
                    $('#paymentStatus').closest('.form-group').addClass('has-success');
                } // /else


                // array validation
                var productName = document.getElementsByName('productName[]');
                var validateProduct;
                for (var x = 0; x < productName.length; x++) {
                    var productNameId = productName[x].id;
                    if(productName[x].value == ''){
                        $("#"+productNameId+"").after('<p class="text-danger"> Product Name Field is required!! </p>');
                        $("#"+productNameId+"").closest('.form-group').addClass('has-error');
                    } else {
                        $("#"+productNameId+"").closest('.form-group').addClass('has-success');
                    }
                } // for

                for (var x = 0; x < productName.length; x++) {
                    if(productName[x].value){
                        validateProduct = true;
                    } else {
                        validateProduct = false;
                    }
                } // for

                var quantity = document.getElementsByName('quantity[]');
                var validateQuantity;
                for (var x = 0; x < quantity.length; x++) {
                    var quantityId = quantity[x].id;
                    if(quantity[x].value == ''){
                        $("#"+quantityId+"").after('<p class="text-danger"> Product Name Field is required!! </p>');
                        $("#"+quantityId+"").closest('.form-group').addClass('has-error');
                    } else {
                        $("#"+quantityId+"").closest('.form-group').addClass('has-success');
                    }
                }  // for

                for (var x = 0; x < quantity.length; x++) {
                    if(quantity[x].value){
                        validateQuantity = true;
                    } else {
                        validateQuantity = false;
                    }
                } // for


                if(orderDate && clientName && clientContact && paid && discount && paymentType && paymentStatus) {
                    if(validateProduct == true && validateQuantity == true) {
                        // create order button
                        // $("#createOrderBtn").button('loading');

                        $.ajax({
                            url : form.attr('action'),
                            type: form.attr('method'),
                            data: form.serialize(),
                            dataType: 'json',
                            success:function(data) {

                             //   alert("Data are saved successfully!!!!!!");

                               // console.log(response);
                                // reset button
                              //  $("#createOrderBtn").button('reset');

                               // $(".text-danger").remove();
                               // $('.form-group').removeClass('has-error').removeClass('has-success');

                           //     if(response.success == true) {

                                    // create order button
                                    $(".success-messages").html('<div class="alert alert-success">'+
                                        '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                        '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ data.messages +

                                        '</div>');
                                    $(".success-messages").fadeOut(800);
                                    $(".success-messages").fadeIn(800);
                                    $(".success-messages").fadeOut(800);
                                    $(".success-messages").fadeIn(800);
                                    $(".success-messages").fadeOut(800);

                                    $("html, body, div.panel, div.pane-body").animate({scrollTop: '0px'}, 100);

                                   //  disabled te modal footer button
                                    $(".submitButtonFooter").addClass('div-hide');
                                    // remove the product row
                                    $(".removeProductRowBtn").addClass('div-hide');

                             //   } else {
                               //     alert(response.messages);
                               // }
                            } // /response
                        }); // /ajax
                    } // if array validate is true
                } // /if field validate is true


                return false;
            }); // /create order form function

        });
    </script>

    <script>
        // print order function
        function printOrder(orderId = null) {
            if(orderId) {

                $.ajax({
                    url: 'printOrder.php',
                    type: 'post',
                    data: {orderId: orderId},
                    dataType: 'text',
                    success:function(response) {

                        var mywindow = window.open('', 'Inventory Management System', 'height=400,width=600');
                        mywindow.document.write('<html><head><title>Order Invoice</title>');
                        mywindow.document.write('</head><body>');
                        mywindow.document.write(response);
                        mywindow.document.write('</body></html>');

                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10

                        mywindow.print();
                        mywindow.close();

                    }// /success function
                }); // /ajax function to fetch the printable order
            } // /if orderId
        } // /print order function

    </script>


    <script>
        // select on product data

        function getProductData(row = null) {

            if(row) {
                var productId = $("#productName"+row).val();

                if(productId == "") {
                    $("#rate"+row).val("");

                    $("#quantity"+row).val("");
                    $("#total"+row).val("");


                } else {
                    $.ajax({

                        url: 'fetchSelectedProduct.php',
                        type: 'post',
                        data: {productId : productId},
                        dataType: 'json',
                        success:function(response) {
                            // setting the rate value into the rate input field

                            $("#rate"+row).val(response.sellPrice);
                            $("#rateValue"+row).val(response.sellPrice);

                            $("#quantity"+row).val(1);

                            var total = Number(response.sellPrice) * 1;
                            total = total.toFixed(2);
                            $("#total"+row).val(total);
                            $("#totalValue"+row).val(total);

                            subAmount();
                        } // /success
                    }); // /ajax function to fetch the product data
                }

            } else {
                alert('no row! please refresh the page');
            }
        }
     // /select on product data

    </script>
    <script>
        function subAmount() {
            var tableProductLength = $("#productTable tbody tr").length;
            var totalSubAmount = 0;
            for(x = 0; x < tableProductLength; x++) {
                var tr = $("#productTable tbody tr")[x];
                var count = $(tr).attr('id');
                count = count.substring(3);

                totalSubAmount = Number(totalSubAmount) + Number($("#total"+count).val());
            } // /for

            totalSubAmount = totalSubAmount.toFixed(2);

            // sub total
            $("#subTotal").val(totalSubAmount);
            $("#subTotalValue").val(totalSubAmount);

            // vat
            var vat = (Number($("#subTotal").val())/100) * 13;
            vat = vat.toFixed(2);
            $("#vat").val(vat);
            $("#vatValue").val(vat);

            // total amount
            var totalAmount = (Number($("#subTotal").val()) + Number($("#vat").val()));
            totalAmount = totalAmount.toFixed(2);
            $("#totalAmount").val(totalAmount);
            $("#totalAmountValue").val(totalAmount);

            var discount = $("#discount").val();
            if(discount) {
                var grandTotal = Number($("#totalAmount").val()) - Number(discount);
                grandTotal = grandTotal.toFixed(2);
                $("#grandTotal").val(grandTotal);
                $("#grandTotalValue").val(grandTotal);
            } else {
                $("#grandTotal").val(totalAmount);
                $("#grandTotalValue").val(totalAmount);
            } // /else discount

            var paidAmount = $("#paid").val();
            if(paidAmount) {
                paidAmount =  Number($("#grandTotal").val()) - Number(paidAmount);
                paidAmount = paidAmount.toFixed(2);
                $("#due").val(paidAmount);
                $("#dueValue").val(paidAmount);
            } else {
                $("#due").val($("#grandTotal").val());
                $("#dueValue").val($("#grandTotal").val());
            } // else

        } // /sub total amount

    </script>

    <script>
        // table total
        function getTotal(row = null) {
            if(row) {
                var total = Number($("#rate"+row).val()) * Number($("#quantity"+row).val());
                total = total.toFixed(2);
                $("#total"+row).val(total);
                $("#totalValue"+row).val(total);

                subAmount();

            } else {
                alert('no row !! please refresh the page');
            }
        }

    </script>

    <script>
        function removeProductRow(row = null) {
            if(row) {
                $("#row"+row).remove();


                subAmount();
            } else {
                alert('error! Refresh the page again');
            }
        }

    </script>


    <script>

        function discountFunc() {
            var discount = $("#discount").val();
            var totalAmount = Number($("#totalAmount").val());
            totalAmount = totalAmount.toFixed(2);

            var grandTotal;
            if(totalAmount) {
                grandTotal = Number($("#totalAmount").val()) - Number($("#discount").val());
                grandTotal = grandTotal.toFixed(2);

                $("#grandTotal").val(grandTotal);
                $("#grandTotalValue").val(grandTotal);
            } else {
            }

            var paid = $("#paid").val();

            var dueAmount;
            if(paid) {
                dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
                dueAmount = dueAmount.toFixed(2);

                $("#due").val(dueAmount);
                $("#dueValue").val(dueAmount);
            } else {
                $("#due").val($("#grandTotal").val());
                $("#dueValue").val($("#grandTotal").val());
            }

        } // /discount function

    </script>

    <script>

        function paidAmount() {
            var grandTotal = $("#grandTotal").val();

            if(grandTotal) {
                var dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
                dueAmount = dueAmount.toFixed(2);
                $("#due").val(dueAmount);
                $("#dueValue").val(dueAmount);
            } // /if
        } // /paid amoutn function


    </script>

<!-- Load TinyMCE -->
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>



<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>