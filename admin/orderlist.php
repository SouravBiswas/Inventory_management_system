﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Customer.php'?>

<?php
  $customer = new Customer();
//$allOrder = $customer->getAllOrder();
//$status = mysqli_fetch_assoc($allOrder);
//$status = $status['payment_status'];
//var_dump($status);
//die();
  $fm = new Format();
if(isset($_GET['delproduct'])){
    $id = $_GET['delproduct'];
    $deleteOrder = $customer->delOrderById($id);
}
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Order List</h2>
        <div class="block" >

            <table  class="data display datatable" id="example">
			<thead>
				<tr style="color: red;font-size: 15px">
					<th>Serial</th>
                    <th>Order Date</th>
                    <th>Client Name</th>
                    <th>Contact</th>
                    <th>Total Order Item</th>
                    <th>Payment Status</th>
                    <th width="20%" style="text-align: center">Action</th>
				</tr>
			</thead>
			<tbody>
            <?php
             $allOrder = $customer->getAllOrder();
             if(mysqli_num_rows($allOrder)>0){
                //$paymentStatus = "";
                 $x = 0;
                 while($row = mysqli_fetch_assoc($allOrder)) {
                     $orderId = $row['order_id'];
                   //  var_dump($orderId);
                   //  die();
                     $countOrderItem = $customer->countOrderItem($orderId);
                 //    $countOrderItem = mysqli_num_rows($countOrderItem);
                   $x++;
            ?>
				<tr class="odd gradeX" >
					<td><?php echo $x?></td>
					<td><?php echo $row['order_date']?></td>
					<td><?php echo $row['client_name']?></td>
					<td class="center"><?php echo $row['client_contact']?></td>
                    <td><?php echo $countOrderItem ?></td>


                    <?php
                        if($row['payment_status'] == 1){
                            echo "<td class='label label-success'>Full Payment</td>";
                        }else if($row['payment_status'] == 2){
                            echo "<td class='label label-info'>Advance Payment</td>";
                        }else{
                            echo "<td class='label label-warning'>No Payment</td>";
                        }
                            ?>



                    <td>
                        <a href="orderedit.php?orderid=<?php echo $orderId ?>" id="editOrderModalBtn">Edit</a> ||
                        <a href="paymentedit.php?paymentid=<?php echo $orderId ?>">Payment</a> ||
                        <a href="#" role="button" onclick="printOrder(<?php echo $orderId?>)">Print</a> ||
                        <a onclick="return confirm('Are you sure to delete!!')" href="?delproduct=<?php echo $orderId;  ?>">Delete</a></td>


                </tr>
            <?php } }?>
			</tbody>
		</table>

       </div>
    </div>
</div>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

<script>
    $( function() {
        $( "#orderDate" ).datepicker();
    } );
</script>

<script>
    $(document).ready(function () {

        // create order form function
        $("#editOrderForm").unbind('submit').bind('submit', function() {

            var form = $(this);

            $('.form-group').removeClass('has-error').removeClass('has-success');
            $('.text-danger').remove();

            var orderDate = $("#orderDate").val();
            var clientName = $("#clientName").val();
            var clientContact = $("#clientContact").val();
            var paid = $("#paid").val();
            var discount = $("#discount").val();
            var paymentType = $("#paymentType").val();
            var paymentStatus = $("#paymentStatus").val();

            // form validation
            if(orderDate == "") {
                $("#orderDate").after('<p class="text-danger"> The Order Date field is required </p>');
                $('#orderDate').closest('.form-group').addClass('has-error');
            } else {
                $('#orderDate').closest('.form-group').addClass('has-success');
            } // /else

            if(clientName == "") {
                $("#clientName").after('<p class="text-danger"> The Client Name field is required </p>');
                $('#clientName').closest('.form-group').addClass('has-error');
            } else {
                $('#clientName').closest('.form-group').addClass('has-success');
            } // /else

            if(clientContact == "") {
                $("#clientContact").after('<p class="text-danger"> The Contact field is required </p>');
                $('#clientContact').closest('.form-group').addClass('has-error');
            } else {
                $('#clientContact').closest('.form-group').addClass('has-success');
            } // /else

            if(paid == "") {
                $("#paid").after('<p class="text-danger"> The Paid field is required </p>');
                $('#paid').closest('.form-group').addClass('has-error');
            } else {
                $('#paid').closest('.form-group').addClass('has-success');
            } // /else

            if(discount == "") {
                $("#discount").after('<p class="text-danger"> The Discount field is required </p>');
                $('#discount').closest('.form-group').addClass('has-error');
            } else {
                $('#discount').closest('.form-group').addClass('has-success');
            } // /else

            if(paymentType == "") {
                $("#paymentType").after('<p class="text-danger"> The Payment Type field is required </p>');
                $('#paymentType').closest('.form-group').addClass('has-error');
            } else {
                $('#paymentType').closest('.form-group').addClass('has-success');
            } // /else

            if(paymentStatus == "") {
                $("#paymentStatus").after('<p class="text-danger"> The Payment Status field is required </p>');
                $('#paymentStatus').closest('.form-group').addClass('has-error');
            } else {
                $('#paymentStatus').closest('.form-group').addClass('has-success');
            } // /else


            // array validation
            var productName = document.getElementsByName('productName[]');
            var validateProduct;
            for (var x = 0; x < productName.length; x++) {
                var productNameId = productName[x].id;
                if(productName[x].value == ''){
                    $("#"+productNameId+"").after('<p class="text-danger"> Product Name Field is required!! </p>');
                    $("#"+productNameId+"").closest('.form-group').addClass('has-error');
                } else {
                    $("#"+productNameId+"").closest('.form-group').addClass('has-success');
                }
            } // for

            for (var x = 0; x < productName.length; x++) {
                if(productName[x].value){
                    validateProduct = true;
                } else {
                    validateProduct = false;
                }
            } // for

            var quantity = document.getElementsByName('quantity[]');
            var validateQuantity;
            for (var x = 0; x < quantity.length; x++) {
                var quantityId = quantity[x].id;
                if(quantity[x].value == ''){
                    $("#"+quantityId+"").after('<p class="text-danger"> Product Name Field is required!! </p>');
                    $("#"+quantityId+"").closest('.form-group').addClass('has-error');
                } else {
                    $("#"+quantityId+"").closest('.form-group').addClass('has-success');
                }
            }  // for

            for (var x = 0; x < quantity.length; x++) {
                if(quantity[x].value){
                    validateQuantity = true;
                } else {
                    validateQuantity = false;
                }
            } // for


            if(orderDate && clientName && clientContact && paid && discount && paymentType && paymentStatus) {
                if(validateProduct == true && validateQuantity == true) {
                    // create order button
                    // $("#createOrderBtn").button('loading');

                    $.ajax({
                        url : form.attr('action'),
                        type: form.attr('method'),
                        data: form.serialize(),
                        dataType: 'json',
                        success:function(data) {

                            //   alert("Data are saved successfully!!!!!!");

                            // console.log(response);
                            // reset button
                            //  $("#createOrderBtn").button('reset');

                            // $(".text-danger").remove();
                            // $('.form-group').removeClass('has-error').removeClass('has-success');

                            //     if(response.success == true) {

                            // create order button
                            $(".success-messages").html('<div class="alert alert-success">'+
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ data.messages +

                                '</div>');
                            $(".success-messages").fadeOut(800);
                            $(".success-messages").fadeIn(800);
                            $(".success-messages").fadeOut(800);
                            $(".success-messages").fadeIn(800);
                            $(".success-messages").fadeOut(800);

                            $("html, body, div.panel, div.pane-body").animate({scrollTop: '0px'}, 100);

                            //  disabled te modal footer button
                            $(".submitButtonFooter").addClass('div-hide');
                            // remove the product row
                            $(".removeProductRowBtn").addClass('div-hide');

                            //   } else {
                            //     alert(response.messages);
                            // }
                        } // /response
                    }); // /ajax
                } // if array validate is true
            } // /if field validate is true


            return false;
        }); // /create order form function

    });
</script>

<script>
    function paymentOrder(orderId = null) {
        if(orderId) {

            $("#orderDate").datepicker();

            $.ajax({
                url: 'fetchOrderData.php',
                type: 'post',
                data: {orderId: orderId},
                dataType: 'json',
                success:function(response) {

                    // due
                    $("#due").val(response.order[10]);

                    // pay amount
                    $("#payAmount").val(response.order[10]);

                    var paidAmount = response.order[9]
                    var dueAmount = response.order[10];
                    var grandTotal = response.order[8];

                    // update payment
                    $("#updatePaymentOrderBtn").unbind('click').bind('click', function() {
                        var payAmount = $("#payAmount").val();
                        var paymentType = $("#paymentType").val();
                        var paymentStatus = $("#paymentStatus").val();

                        if(payAmount == "") {
                            $("#payAmount").after('<p class="text-danger">The Pay Amount field is required</p>');
                            $("#payAmount").closest('.form-group').addClass('has-error');
                        } else {
                            $("#payAmount").closest('.form-group').addClass('has-success');
                        }

                        if(paymentType == "") {
                            $("#paymentType").after('<p class="text-danger">The Pay Amount field is required</p>');
                            $("#paymentType").closest('.form-group').addClass('has-error');
                        } else {
                            $("#paymentType").closest('.form-group').addClass('has-success');
                        }

                        if(paymentStatus == "") {
                            $("#paymentStatus").after('<p class="text-danger">The Pay Amount field is required</p>');
                            $("#paymentStatus").closest('.form-group').addClass('has-error');
                        } else {
                            $("#paymentStatus").closest('.form-group').addClass('has-success');
                        }

                        if(payAmount && paymentType && paymentStatus) {
                            $("#updatePaymentOrderBtn").button('loading');
                            $.ajax({
                                url: 'php_action/editPayment.php',
                                type: 'post',
                                data: {
                                    orderId: orderId,
                                    payAmount: payAmount,
                                    paymentType: paymentType,
                                    paymentStatus: paymentStatus,
                                    paidAmount: paidAmount,
                                    grandTotal: grandTotal
                                },
                                dataType: 'json',
                                success:function(response) {
                                    $("#updatePaymentOrderBtn").button('loading');

                                    // remove error
                                    $('.text-danger').remove();
                                    $('.form-group').removeClass('has-error').removeClass('has-success');

                                    $("#paymentOrderModal").modal('hide');

                                    $("#success-messages").html('<div class="alert alert-success">'+
                                        '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                        '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
                                        '</div>');

                                    // remove the mesages
                                    $(".alert-success").delay(500).show(10, function() {
                                        $(this).delay(3000).hide(10, function() {
                                            $(this).remove();
                                        });
                                    }); // /.alert

                                    // refresh the manage order table
                                    manageOrderTable.ajax.reload(null, false);

                                } //

                            });
                        } // /if

                        return false;
                    }); // /update payment

                } // /success
            }); // fetch order data
        } else {
            alert('Error ! Refresh the page again');
        }
    }

</script>

<script>
    // print order function
    function printOrder(orderId = null) {
        if(orderId) {

            $.ajax({
                url: 'printOrder.php',
                type: 'post',
                data: {orderId: orderId},
                dataType: 'text',
                success:function(response) {

                    var mywindow = window.open('', 'Inventory Management System', 'height=400,width=600');
                    mywindow.document.write('<html><head><title>Order Invoice</title>');
                    mywindow.document.write('</head><body>');
                    mywindow.document.write(response);
                    mywindow.document.write('</body></html>');

                    mywindow.document.close(); // necessary for IE >= 10
                    mywindow.focus(); // necessary for IE >= 10

                    mywindow.print();
                    mywindow.close();

                }// /success function
            }); // /ajax function to fetch the printable order
        } // /if orderId
    } // /print order function

</script>


<script>
    // select on product data

    function getProductData(row = null) {

        if(row) {
            var productId = $("#productName"+row).val();

            if(productId == "") {
                $("#rate"+row).val("");

                $("#quantity"+row).val("");
                $("#total"+row).val("");


            } else {
                $.ajax({

                    url: 'fetchSelectedProduct.php',
                    type: 'post',
                    data: {productId : productId},
                    dataType: 'json',
                    success:function(response) {
                        // setting the rate value into the rate input field

                        $("#rate"+row).val(response.sellPrice);
                        $("#rateValue"+row).val(response.sellPrice);

                        $("#quantity"+row).val(1);

                        var total = Number(response.sellPrice) * 1;
                        total = total.toFixed(2);
                        $("#total"+row).val(total);
                        $("#totalValue"+row).val(total);

                        subAmount();
                    } // /success
                }); // /ajax function to fetch the product data
            }

        } else {
            alert('no row! please refresh the page');
        }
    }
    // /select on product data

</script>
<script>
    function subAmount() {
        var tableProductLength = $("#productTable tbody tr").length;
        var totalSubAmount = 0;
        for(x = 0; x < tableProductLength; x++) {
            var tr = $("#productTable tbody tr")[x];
            var count = $(tr).attr('id');
            count = count.substring(3);

            totalSubAmount = Number(totalSubAmount) + Number($("#total"+count).val());
        } // /for

        totalSubAmount = totalSubAmount.toFixed(2);

        // sub total
        $("#subTotal").val(totalSubAmount);
        $("#subTotalValue").val(totalSubAmount);

        // vat
        var vat = (Number($("#subTotal").val())/100) * 13;
        vat = vat.toFixed(2);
        $("#vat").val(vat);
        $("#vatValue").val(vat);

        // total amount
        var totalAmount = (Number($("#subTotal").val()) + Number($("#vat").val()));
        totalAmount = totalAmount.toFixed(2);
        $("#totalAmount").val(totalAmount);
        $("#totalAmountValue").val(totalAmount);

        var discount = $("#discount").val();
        if(discount) {
            var grandTotal = Number($("#totalAmount").val()) - Number(discount);
            grandTotal = grandTotal.toFixed(2);
            $("#grandTotal").val(grandTotal);
            $("#grandTotalValue").val(grandTotal);
        } else {
            $("#grandTotal").val(totalAmount);
            $("#grandTotalValue").val(totalAmount);
        } // /else discount

        var paidAmount = $("#paid").val();
        if(paidAmount) {
            paidAmount =  Number($("#grandTotal").val()) - Number(paidAmount);
            paidAmount = paidAmount.toFixed(2);
            $("#due").val(paidAmount);
            $("#dueValue").val(paidAmount);
        } else {
            $("#due").val($("#grandTotal").val());
            $("#dueValue").val($("#grandTotal").val());
        } // else

    } // /sub total amount

</script>

<script>
    // table total
    function getTotal(row = null) {
        if(row) {
            var total = Number($("#rate"+row).val()) * Number($("#quantity"+row).val());
            total = total.toFixed(2);
            $("#total"+row).val(total);
            $("#totalValue"+row).val(total);

            subAmount();

        } else {
            alert('no row !! please refresh the page');
        }
    }

</script>

<script>
    function removeProductRow(row = null) {
        if(row) {
            $("#row"+row).remove();


            subAmount();
        } else {
            alert('error! Refresh the page again');
        }
    }

</script>


<script>

    function discountFunc() {
        var discount = $("#discount").val();
        var totalAmount = Number($("#totalAmount").val());
        totalAmount = totalAmount.toFixed(2);

        var grandTotal;
        if(totalAmount) {
            grandTotal = Number($("#totalAmount").val()) - Number($("#discount").val());
            grandTotal = grandTotal.toFixed(2);

            $("#grandTotal").val(grandTotal);
            $("#grandTotalValue").val(grandTotal);
        } else {
        }

        var paid = $("#paid").val();

        var dueAmount;
        if(paid) {
            dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
            dueAmount = dueAmount.toFixed(2);

            $("#due").val(dueAmount);
            $("#dueValue").val(dueAmount);
        } else {
            $("#due").val($("#grandTotal").val());
            $("#dueValue").val($("#grandTotal").val());
        }

    } // /discount function

</script>

<script>

    function paidAmount() {
        var grandTotal = $("#grandTotal").val();

        if(grandTotal) {
            var dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
            dueAmount = dueAmount.toFixed(2);
            $("#due").val(dueAmount);
            $("#dueValue").val(dueAmount);
        } // /if
    } // /paid amoutn function


</script>

<!-- Load TinyMCE -->
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>





<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
