<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php';?>
<?php include '../classes/Customer.php';?>
<?php
$pd = new Product();
$customer = new Customer();

if(!isset($_GET['paymentid']) || $_GET['paymentid'] == NULL){
    echo "<script>window.location = 'orderlist.php'</script>";
}else{

    $paymentid = $_GET['paymentid'];
}
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $updatePayment = $customer->paymentUpdate($_POST,$paymentid);
}


?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Payment</h2>
    <div class="block">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-plus"></i>Update Payment
            </div>
            <br>
            <div  class="message">
                <?php
                if(isset($updatePayment)){
                   // echo $updatePayment;
                    echo "<h4 style='color: green'><script>alert('Payment Updated Successfully!!')</script></h4>";
                    echo "<script>window.location = 'orderlist.php'</script>";
                }

                ?>

            </div>
            <?php
            $paymentData = $customer->getPaymentById($paymentid);
            if($paymentData){
                while($data = mysqli_fetch_assoc($paymentData)){

            ?>
        <form class="form-horizontal" method="POST" action="" id="createOrderForm">

            <div class="col-md-6">

                <div class="form-group">
                    <label for="due" class="col-sm-3 control-label">Due Amount</label>
                    <div class="col-sm-9">
                        <input type="text" value="<?php echo $data['due']?>" class="form-control" id="due" name="due" disabled="true" />
                        <input type="hidden" value="<?php echo $data['due']?>" class="form-control" id="dueValue" name="dueValue" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="paid" class="col-sm-3 control-label">Paid Amount</label>
                    <div class="col-sm-9">
                        <input type="text" value="<?php echo $data['due']?>" class="form-control" id="paid" name="paid" autocomplete="off" onkeyup="paidAmount()" />
                    </div>
                </div> <!--/form-group-->

                <!--/form-group-->
                <div class="form-group">
                    <label for="clientContact" class="col-sm-3 control-label">Payment Type</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="paymentType" id="paymentType">
                            <option value="">~~SELECT~~</option>
                            <option value="1">Cheque</option>
                            <option value="2">Cash</option>
                            <option value="3">Credit Card</option>
                        </select>
                    </div>
                </div> <!--/form-group-->
                <div class="form-group">
                    <label for="clientContact" class="col-sm-3 control-label">Payment Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="paymentStatus" id="paymentStatus">
                            <option value="">~~SELECT~~</option>
                            <option value="1">Full Payment</option>
                            <option value="2">Advance Payment</option>
                            <option value="3">No Payment</option>
                        </select>
                    </div>
                </div> <!--/form-group-->
            </div> <!--/col-md-6-->


            <div class="form-group submitButtonFooter">
                <div class="col-sm-offset-2 col-sm-10">

                    <input type="hidden" name="paymentId" id="orderId" value="<?php echo $_GET['paymentid']; ?>" />

                    <button type="submit" name="submit" id="createOrderBtn" data-loading-text="Loading..." class="btn btn-success"><i class=""></i> Save Changes</button>

                    <button type="reset" class="btn btn-default" onclick="resetOrderForm()"><i class=""></i> Reset</button>
                </div>
            </div>

        </form>
            <?php } }?>
        </div>
    </div>
    </div>
</div>


    <script>
        $( function() {
            $( "#orderDate" ).datepicker();
        } );
    </script>

    <script>
        $(document).ready(function () {

            // create order form function
            $("#createOrderForm").unbind('submit').bind('submit', function() {

                var form = $(this);

                $('.form-group').removeClass('has-error').removeClass('has-success');
                $('.text-danger').remove();

                var paid = $("#paid").val();
                var paymentType = $("#paymentType").val();
                var paymentStatus = $("#paymentStatus").val();

                // form validation
                if(paid == "") {
                    $("#paid").after('<p class="text-danger"> The Paid field is required </p>');
                    $('#paid').closest('.form-group').addClass('has-error');
                } else {
                    $('#paid').closest('.form-group').addClass('has-success');
                } // /else

                if(paymentType == "") {
                    $("#paymentType").after('<p class="text-danger"> The Payment Type field is required </p>');
                    $('#paymentType').closest('.form-group').addClass('has-error');
                } else {
                    $('#paymentType').closest('.form-group').addClass('has-success');
                } // /else

                if(paymentStatus == "") {
                    $("#paymentStatus").after('<p class="text-danger"> The Payment Status field is required </p>');
                    $('#paymentStatus').closest('.form-group').addClass('has-error');
                } else {
                    $('#paymentStatus').closest('.form-group').addClass('has-success');
                } // /else


                // array validation
                if( paid &&  paymentType && paymentStatus) {
                    return true;
                }else {
                    return false;
                }





                //  return false;
            }); // /create order form function

        });
    </script>




    <script>
        // Payment ORDER
        function paymentOrder(orderId = null) {
            if(orderId) {

                $("#orderDate").datepicker();

                $.ajax({
                    url: 'php_action/fetchOrderData.php',
                    type: 'post',
                    data: {orderId: orderId},
                    dataType: 'json',
                    success:function(response) {

                        // due
                        $("#due").val(response.order[10]);

                        // pay amount
                        $("#payAmount").val(response.order[10]);

                        var paidAmount = response.order[9]
                        var dueAmount = response.order[10];
                        var grandTotal = response.order[8];

                        // update payment
                        $("#updatePaymentOrderBtn").unbind('click').bind('click', function() {
                            var payAmount = $("#payAmount").val();
                            var paymentType = $("#paymentType").val();
                            var paymentStatus = $("#paymentStatus").val();

                            if(payAmount == "") {
                                $("#payAmount").after('<p class="text-danger">The Pay Amount field is required</p>');
                                $("#payAmount").closest('.form-group').addClass('has-error');
                            } else {
                                $("#payAmount").closest('.form-group').addClass('has-success');
                            }

                            if(paymentType == "") {
                                $("#paymentType").after('<p class="text-danger">The Pay Amount field is required</p>');
                                $("#paymentType").closest('.form-group').addClass('has-error');
                            } else {
                                $("#paymentType").closest('.form-group').addClass('has-success');
                            }

                            if(paymentStatus == "") {
                                $("#paymentStatus").after('<p class="text-danger">The Pay Amount field is required</p>');
                                $("#paymentStatus").closest('.form-group').addClass('has-error');
                            } else {
                                $("#paymentStatus").closest('.form-group').addClass('has-success');
                            }

                            if(payAmount && paymentType && paymentStatus) {
                                $("#updatePaymentOrderBtn").button('loading');
                                $.ajax({
                                    url: 'php_action/editPayment.php',
                                    type: 'post',
                                    data: {
                                        orderId: orderId,
                                        payAmount: payAmount,
                                        paymentType: paymentType,
                                        paymentStatus: paymentStatus,
                                        paidAmount: paidAmount,
                                        grandTotal: grandTotal
                                    },
                                    dataType: 'json',
                                    success:function(response) {
                                        $("#updatePaymentOrderBtn").button('loading');

                                        // remove error
                                        $('.text-danger').remove();
                                        $('.form-group').removeClass('has-error').removeClass('has-success');

                                        $("#paymentOrderModal").modal('hide');

                                        $("#success-messages").html('<div class="alert alert-success">'+
                                            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
                                            '</div>');

                                        // remove the mesages
                                        $(".alert-success").delay(500).show(10, function() {
                                            $(this).delay(3000).hide(10, function() {
                                                $(this).remove();
                                            });
                                        }); // /.alert

                                        // refresh the manage order table
                                        manageOrderTable.ajax.reload(null, false);

                                    } //

                                });
                            } // /if

                            return false;
                        }); // /update payment

                    } // /success
                }); // fetch order data
            } else {
                alert('Error ! Refresh the page again');
            }
        }

    </script>

    <script>
        // print order function
        function printOrder(orderId = null) {
            if(orderId) {

                $.ajax({
                    url: 'printOrder.php',
                    type: 'post',
                    data: {orderId: orderId},
                    dataType: 'text',
                    success:function(response) {

                        var mywindow = window.open('', 'Inventory Management System', 'height=400,width=600');
                        mywindow.document.write('<html><head><title>Order Invoice</title>');
                        mywindow.document.write('</head><body>');
                        mywindow.document.write(response);
                        mywindow.document.write('</body></html>');

                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10

                        mywindow.print();
                        mywindow.close();

                    }// /success function
                }); // /ajax function to fetch the printable order
            } // /if orderId
        } // /print order function

    </script>


    <script>
        // select on product data

        function getProductData(row = null) {

            if(row) {
                var productId = $("#productName"+row).val();

                if(productId == "") {
                    $("#rate"+row).val("");

                    $("#quantity"+row).val("");
                    $("#total"+row).val("");


                } else {
                    $.ajax({

                        url: 'fetchSelectedProduct.php',
                        type: 'post',
                        data: {productId : productId},
                        dataType: 'json',
                        success:function(response) {
                            // setting the rate value into the rate input field

                            $("#rate"+row).val(response.sellPrice);
                            $("#rateValue"+row).val(response.sellPrice);

                            $("#quantity"+row).val(1);

                            var total = Number(response.sellPrice) * 1;
                            total = total.toFixed(2);
                            $("#total"+row).val(total);
                            $("#totalValue"+row).val(total);

                            subAmount();
                        } // /success
                    }); // /ajax function to fetch the product data
                }

            } else {
                alert('no row! please refresh the page');
            }
        }
     // /select on product data

    </script>
    <script>
        function subAmount() {
            var tableProductLength = $("#productTable tbody tr").length;
            var totalSubAmount = 0;
            for(x = 0; x < tableProductLength; x++) {
                var tr = $("#productTable tbody tr")[x];
                var count = $(tr).attr('id');
                count = count.substring(3);

                totalSubAmount = Number(totalSubAmount) + Number($("#total"+count).val());
            } // /for

            totalSubAmount = totalSubAmount.toFixed(2);

            // sub total
            $("#subTotal").val(totalSubAmount);
            $("#subTotalValue").val(totalSubAmount);

            // vat
            var vat = (Number($("#subTotal").val())/100) * 13;
            vat = vat.toFixed(2);
            $("#vat").val(vat);
            $("#vatValue").val(vat);

            // total amount
            var totalAmount = (Number($("#subTotal").val()) + Number($("#vat").val()));
            totalAmount = totalAmount.toFixed(2);
            $("#totalAmount").val(totalAmount);
            $("#totalAmountValue").val(totalAmount);

            var discount = $("#discount").val();
            if(discount) {
                var grandTotal = Number($("#totalAmount").val()) - Number(discount);
                grandTotal = grandTotal.toFixed(2);
                $("#grandTotal").val(grandTotal);
                $("#grandTotalValue").val(grandTotal);
            } else {
                $("#grandTotal").val(totalAmount);
                $("#grandTotalValue").val(totalAmount);
            } // /else discount

            var paidAmount = $("#paid").val();
            if(paidAmount) {
                paidAmount =  Number($("#grandTotal").val()) - Number(paidAmount);
                paidAmount = paidAmount.toFixed(2);
                $("#due").val(paidAmount);
                $("#dueValue").val(paidAmount);
            } else {
                $("#due").val($("#grandTotal").val());
                $("#dueValue").val($("#grandTotal").val());
            } // else

        } // /sub total amount

    </script>

    <script>
        // table total
        function getTotal(row = null) {
            if(row) {
                var total = Number($("#rate"+row).val()) * Number($("#quantity"+row).val());
                total = total.toFixed(2);
                $("#total"+row).val(total);
                $("#totalValue"+row).val(total);

                subAmount();

            } else {
                alert('no row !! please refresh the page');
            }
        }

    </script>

    <script>
        function removeProductRow(row = null) {
            if(row) {
                $("#row"+row).remove();


                subAmount();
            } else {
                alert('error! Refresh the page again');
            }
        }

    </script>


    <script>

        function discountFunc() {
            var discount = $("#discount").val();
            var totalAmount = Number($("#totalAmount").val());
            totalAmount = totalAmount.toFixed(2);

            var grandTotal;
            if(totalAmount) {
                grandTotal = Number($("#totalAmount").val()) - Number($("#discount").val());
                grandTotal = grandTotal.toFixed(2);

                $("#grandTotal").val(grandTotal);
                $("#grandTotalValue").val(grandTotal);
            } else {
            }

            var paid = $("#paid").val();

            var dueAmount;
            if(paid) {
                dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
                dueAmount = dueAmount.toFixed(2);

                $("#due").val(dueAmount);
                $("#dueValue").val(dueAmount);
            } else {
                $("#due").val($("#grandTotal").val());
                $("#dueValue").val($("#grandTotal").val());
            }

        } // /discount function

    </script>

    <script>

        function paidAmount() {
            var grandTotal = $("#grandTotal").val();

            if(grandTotal) {
                var dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
                dueAmount = dueAmount.toFixed(2);
                $("#due").val(dueAmount);
                $("#dueValue").val(dueAmount);
            } // /if
        } // /paid amoutn function


    </script>

<!-- Load TinyMCE -->
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>



<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>