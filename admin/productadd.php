﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php'?>
<?php include '../classes/Brand.php'?>
<?php include '../classes/Category.php'?>
<?php
$product = new Product();
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $insertProduct = $product->productInsert($_POST,$_FILES);
}

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Product</h2>
        <div class="block">
            <?php
            if(isset($insertProduct)){
                echo $insertProduct;
            }
            ?>
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
               
                <tr>
                    <td>
                        <label>Name</label>
                    </td>
                    <td>
                        <input type="text" name="productName" placeholder="Enter Product Name..." class="medium" />
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Category</label>
                    </td>
                    <td>
                        <select id="select" name="catId">
                            <option>Select Category</option>
                            <?php
                            $cat = new Category();
                            $catAll = $cat->getAllCat();
                            if($catAll){
                                while ($result = mysqli_fetch_assoc($catAll)){

                            ?>
                            <option value="<?php echo $result['catId']?>"><?php echo $result['catName']?></option>
                            <?php } }?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Brand</label>
                    </td>
                    <td>
                        <select id="select" name="brandId">
                            <option>Select Brand</option>
                            <?php
                            $brand = new Brand();
                            $brandAll = $brand->getAllBrand();
                            if($brandAll){
                                while ($result = mysqli_fetch_assoc($brandAll)){
                            ?>
                            <option value="<?php echo $result['brandId']?>"><?php echo $result['brandName']?></option>
                            <?php } }?>
                        </select>
                    </td>
                </tr>
				
				 <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Quantity</label>
                    </td>
                    <td>
                        <input type="text" name="quantity" placeholder="Enter Quantity..." class="medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Buy Price</label>
                    </td>
                    <td>
                        <input type="text" name="buyPrice" placeholder="Enter buy Price..." class="medium" />
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Sell Price</label>
                    </td>
                    <td>
                        <input type="text" name="sellPrice" placeholder="Enter sell Price..." class="medium" />
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <label>Upload Image</label>
                    </td>
                    <td>
                        <input type="file" name="image" />
                    </td>
                </tr>
				
				<tr>
                    <td>
                        <label>Status</label>
                    </td>
                    <td>
                        <select id="select" name="productStatus">
                            <option>Select Status</option>
                            <option value="1">Available</option>
                            <option value="2">Not Available</option>
                        </select>
                    </td>
                </tr>

				<tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Save" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


