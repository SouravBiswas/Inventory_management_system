﻿ <?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php'?>
<?php include '../classes/Brand.php'?>
<?php include '../classes/Category.php'?>
<?php
if(!isset($_GET['productid']) || $_GET['productid'] == NULL){
    echo "<script>window.location = 'productlist.php'</script>";
}else{

    $id = $_GET['productid'];
}
$product = new Product();
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $updateProduct = $product->productUpdate($_POST,$_FILES,$id);
}

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Product</h2>
        <div class="block">
            <?php
            if(isset($updateProduct)){
                echo $updateProduct;
            }
            ?>
            <?php
            $getProduct = $product->getProductById($id);
            if($getProduct){
                while ($value = mysqli_fetch_assoc($getProduct)){

            ?>
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
               
                <tr>
                    <td>
                        <label>Name</label>
                    </td>
                    <td>
                        <input type="text" name="productName" value="<?php echo $value['productName']?>" class="medium" />
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Category</label>
                    </td>
                    <td>
                        <select id="select" name="catId">
                            <option>Select Category</option>
                            <?php
                            $cat = new Category();
                            $catAll = $cat->getAllCat();
                            if($catAll){
                                while ($result = mysqli_fetch_assoc($catAll)){

                            ?>
                            <option
                                    <?php
                                    if($value['catId'] == $result['catId']){?>
                                      selected="selected"
                                 <?php   }?>

                                    value="<?php echo $result['catId']?>"><?php echo $result['catName']?></option>
                            <?php } }?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Brand</label>
                    </td>
                    <td>
                        <select id="select" name="brandId">
                            <option>Select Brand</option>
                            <?php
                            $brand = new Brand();
                            $brandAll = $brand->getAllBrand();
                            if($brandAll){
                                while ($result = mysqli_fetch_assoc($brandAll)){
                            ?>
                                    <option
                                        <?php
                                        if($value['brandId'] == $result['brandId']){?>
                                            selected="selected"
                                        <?php   }?>

                                            value="<?php echo $result['brandId']?>"><?php echo $result['brandName']?></option>
                                <?php } }?>
                        </select>
                    </td>
                </tr>
				
				 <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Quantity</label>
                    </td>
                     <td>
                         <input type="text" name="quantity" value="<?php echo $value['quantity']?>" class="medium" />
                     </td>
                </tr>
				<tr>
                    <td>
                        <label>Price</label>
                    </td>
                    <td>
                        <input type="text" name="price" value="<?php echo $value['sellPrice']?>" class="medium" />
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <label>Upload Image</label>
                    </td>
                    <td>
                        <img src="<?php echo $value['image']?>" height="80px" width="200px"/><br/>
                        <input type="file" name="image" />
                    </td>
                </tr>
				
				<tr>
                    <td>
                        <label>Product Type</label>
                    </td>
                    <td>
                        <select id="select" name="productStatus">
                            <option>Select Status</option>
                            <?php if($value['status'] == 1){?>
                            <option selected="selected" value="1">Available</option>
                            <option value="2">Not Available</option>
                        <?php } else {?>
                        <option selected="selected" value="2">Not Available</option>
                        <option value="1">Available</option>
                        <?php }?>
                        </select>
                    </td>
                </tr>

				<tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Update" />
                    </td>
                </tr>
            </table>
            </form>
            <?php } }?>
        </div>
    </div>
</div>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


