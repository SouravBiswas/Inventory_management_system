﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php';?>
<?php
$pd = new Product();
//$insertProduct = '';
//if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
// $insertProduct = $pd->orderInsert($_POST);
//var_dump($insertProduct);
//die();
//}


?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Order Report</h2>
            <div class="block">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-plus"></i>Order Report
                    </div>
                    <br>
                    <div  class="success-messages"></div>
                    <form class="form-horizontal" action="getOrderReport.php" method="post" id="getOrderReportForm">
                        <div class="form-group">
                            <label for="startDate" class="col-sm-2 control-label">Start Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="startDate" name="startDate" placeholder="Start Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="endDate" class="col-sm-2 control-label">End Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="endDate" name="endDate" placeholder="End Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" id="generateReportBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Generate Report</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <script src="custom/js/report.js"></script>

    <!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>