<?php include '../classes/Brand.php'?>
<?php include '../classes/Product.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$brand = new Brand();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $supplierName = $_POST['supplierName'];
    $supplierContact = $_POST['supplierContact'];
    $supplierProduct = $_POST['supplierProduct'];
    $insertSupplier = $brand->supplierInsert($supplierName,$supplierContact,$supplierProduct);
}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Supplier</h2>
            <div class="block copyblock">
                <?php
                if(isset($insertSupplier)){
                    echo $insertSupplier;
                }
                ?>
                <form action="" method="post">
                    <table class="form" style="height: 180px">
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="supplierName" placeholder="Enter Supplier Name..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="supplierContact" placeholder="Enter Contact Number..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 350px" class="form-control" id="supplierStatus" name="supplierProduct">
                                    <option value="">~~Select Product~~</option>
                                    <?php
                                    $product = new Product();
                                    $allProduct = $product->getAllProduct();
                                    if($allProduct){
                                        while ($result = mysqli_fetch_assoc($allProduct)){
                                            ?>
                                            <option value="<?php echo $result['productId']?>"><?php echo $result['productName']?></option>
                                        <?php } }?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>