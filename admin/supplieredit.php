<?php include '../classes/Brand.php'?>
<?php include '../classes/Product.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
if(!isset($_GET['supplierid']) || $_GET['supplierid'] == NULL){
    echo "<script>window.location = 'supplierlist.php'</script>";
}else{

    $id = $_GET['supplierid'];
}
$brand = new Brand();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //$id=$_GET['catid'];
    $supplierName = $_POST['supplierName'];
    $supplierContact = $_POST['supplierContact'];
    $supplierProduct = $_POST['supplierProduct'];
    $updateSupplier = $brand->supplierUpdate($supplierName,$supplierContact,$supplierProduct,$id);

}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Supplier Data</h2>
            <div class="block copyblock">
                <?php
                if(isset($updateSupplier)){
                    echo $updateSupplier;
                }
                ?>
      <?php
         $getSupplier = $brand->getSupplierById($id);
         if($getSupplier){

         while ($result = mysqli_fetch_assoc($getSupplier)){

        ?>

        <form action="" method="post">
                    <table class="form" style="height: 180px">
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="supplierName" value="<?php echo $result['supplier_name']?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input style="width: 318px" type="text" name="supplierContact" value="<?php echo $result['supplier_contact']?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select style="width: 318px" class="form-control" id="brandStatus" name="supplierProduct">
                                    <option>~~Select Product~~</option>
                                    <?php
                                    $product = new Product();
                                    $getProduct = $product->getAllProduct();
                                    if($getProduct){
                                        while ($value = mysqli_fetch_assoc($getProduct)){

                                            ?>
                                            <option
                                                <?php
                                                if($result['supplier_product'] == $value['productId']){?>
                                                    selected="selected"
                                                <?php   }?>

                                                    value="<?php echo $value['productId']?>"><?php echo $value['productName']?></option>
                                        <?php } }?>
                                                    </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
             <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>