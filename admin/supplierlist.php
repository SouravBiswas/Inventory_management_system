﻿<?php include '../classes/Brand.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php
   $supplierList = new Brand();
   if(isset($_GET['delsupplier'])){
     $id = $_GET['delsupplier'];
     $deleteSupplier = $supplierList->delSupplierById($id);
}
   $getAllSupplier = $supplierList->getAllSupplier();
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Supplier List</h2>
                <div class="block">
                    <?php
                    if(isset($deleteSupplier)){
                        echo $deleteSupplier;
                    }
                    ?>
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Supplier Name</th>
                            <th>Contact No.</th>
                            <th>Supplied Product</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    if($getAllSupplier){
                        $i=0;
                        while ($result = mysqli_fetch_assoc($getAllSupplier)){
                            $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo $i?></td>
							<td><?php echo $result['supplier_name']?></td>
                            <td><?php echo $result['supplier_contact']?></td>
                                <?php
                                $productId = $result['supplier_product'];
                                $getSupplierProduct = $supplierList->getSupplierProduct($productId);
                                if($getSupplierProduct){
                                    while ($supplierProduct = mysqli_fetch_assoc($getSupplierProduct)){
                                        $product = $supplierProduct['productName'];
                                        echo "<td class=''>$product</td>";
                                    }

                                }

                                ?>

							<td><a href="supplieredit.php?supplierid=<?php echo $result['supplier_id'] ?>">Edit</a> ||
                                <a onclick="return confirm('Are you sure to delete!!')" href="?delsupplier=<?php echo $result['supplier_id'];  ?>">Delete</a></td>
						</tr>
                    <?php } }?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

