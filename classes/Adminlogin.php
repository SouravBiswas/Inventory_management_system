<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Session.php');
//include '../lib/Session.php';
Session::checkLogin();

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

?>

<?php


class Adminlogin
{

    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function adminLogin($adminUser,$adminPass){

        $adminUser = $this->fm->validation($adminUser);
        $adminPass = $this->fm->validation($adminPass);

        $adminUser = mysqli_real_escape_string($this->db->link,$adminUser);
        $adminPass = mysqli_real_escape_string($this->db->link,$adminPass);

        if(empty($adminUser) || empty($adminPass)){

            $loginmsg = "Username or Password must not be empty";
            return $loginmsg;
        }else{

            $query = "select * from tbl_admin WHERE adminUser='$adminUser' AND adminPass='$adminPass'";
            $query = $this->db->select($query);
            $result = mysqli_num_rows($query);

            if($result){
                $value = mysqli_fetch_assoc($query);
                Session::set("adminlogin",true);
                Session::set("adminId",$value['adminId']);
                Session::set("adminUser",$value['adminUser']);
                Session::set("adminName",$value['adminName']);
                Session::set("adminEmail",$value['adminEmail']);
                Session::set("adminPass",$value['adminPass']);
                header("Location:index.php");
            }else{
                $loginmsg = "Username or Password not match";
                return $loginmsg;
            }
        }
    }
}
?>