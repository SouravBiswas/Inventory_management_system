<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

/*
  Category class ..
 */

class Category
{
    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function catInsert($catName,$catStatus){

        $catName = $this->fm->validation($catName);
        $catStatus = $this->fm->validation($catStatus);

        $catName = mysqli_real_escape_string($this->db->link,$catName);
        $catStatus = mysqli_real_escape_string($this->db->link,$catStatus);
        if(empty($catName) || empty($catStatus)){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{

            $query = "insert into tbl_category(catName,cat_active,cat_status) VALUES ('$catName','$catStatus',1)";
            $catInsert = $this->db->insert($query);
            if($catInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Category Inserted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Category not Inserted !!</span></center>";
                return $msg;
            }
        }

    }

    public function getAllCat(){

        $query = "select * from tbl_category order by catId DESC ";
        $result = $this->db->select($query);

        return $result;
    }

    public function getcatById($id){

        $query = "select * from tbl_category where catId = '$id'";
        $result = $this->db->select($query);

        return $result;
    }

    public function catUpdate($catName,$catStatus,$id){

        $catName = $this->fm->validation($catName);
        $catStatus = $this->fm->validation($catStatus);

        $catName = mysqli_real_escape_string($this->db->link,$catName);
        $catStatus = mysqli_real_escape_string($this->db->link,$catStatus);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if(empty($catName) || empty($catStatus)){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{

            $query = "update tbl_category set catName='$catName',cat_active = '$catStatus' where catId='$id'";
            $result = $this->db->update($query);
            if($result){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Category Updated Successfully !!</span></center>";
                return $msg;
            }
            else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Category not Updated !!</span></center>";
                return $msg;
            }


        }
    }

    public function delCatById($id){

        $query = "delete from tbl_category where catId = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Category Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Category not Deleted !!</span></center>";
            return $msg;
        }
    }
}