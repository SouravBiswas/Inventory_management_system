<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

class Product
{
    private $db;
    private $fm;

    public function __construct()
    {

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function productInsert($data, $file)
    {

        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catId = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $quantity = mysqli_real_escape_string($this->db->link, $data['quantity']);
        $buyPrice = mysqli_real_escape_string($this->db->link, $data['buyPrice']);
        $sellPrice = mysqli_real_escape_string($this->db->link, $data['sellPrice']);
        $productStatus = mysqli_real_escape_string($this->db->link, $data['productStatus']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;
        if ($productName == "" || $catId == "" || $brandId == "" || $quantity == "" || $buyPrice == "" || $sellPrice == "" || $file_name == "" || $productStatus == "") {
            $msg = "<span class='error' style='font-weight: bold'>Fields must not be empty !!</span>";
            return $msg;
        } else {
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "insert into tbl_product(productName,catId,brandId,quantity,buyPrice,sellPrice,image,status) values('$productName','$catId','$brandId','$quantity','$buyPrice','$sellPrice','$uploaded_image','$productStatus')";
            $productInsert = $this->db->insert($query);
            if ($productInsert) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Product Inserted Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Product not Inserted !!</span></center>";
                return $msg;
            }
        }
    }

    public function getAllProduct()
    {

        $query = "select tbl_product.*,tbl_category.catName,tbl_brands.brandName
                  from tbl_product
                  inner join tbl_category
                  on tbl_product.catId = tbl_category.catId
                  inner join tbl_brands
                  on tbl_product.brandId = tbl_brands.brandId
                  order by tbl_product.productId desc";
        $result = $this->db->select($query);
        return $result;
    }

    public function getProductById($id)
    {
        $query = "select * from tbl_product where productId = '$id'";
        $result = $this->db->select($query);

        return $result;
    }

    public function productUpdate($data, $file, $id)
    {
        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catId = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $quantity = mysqli_real_escape_string($this->db->link, $data['quantity']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $productStatus = mysqli_real_escape_string($this->db->link, $data['productStatus']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;
        if ($productName == "" || $catId == "" || $brandId == "" || $quantity == "" || $price == "" || $productStatus == "") {
            $msg = "<span class='error' style='font-weight: bold'>Fields must not be empty !!</span>";
            return $msg;
        } else {
            if(!empty($file_name)) {

                if ($file_size > 1048567) {

                    echo "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>
            Image size should be less then 1MB!!
            </span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);
                    $query = "update tbl_product set productName='$productName',catId='$catId',brandId='$brandId',quantity='$quantity',sellPrice='$price',image='$uploaded_image',status='$productStatus' where productId='$id'";
                    $productUpdate = $this->db->update($query);
                    if ($productUpdate) {
                        $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Product Updated Successfully!!</span></center>";
                        return $msg;
                    } else {
                        $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Product not Updated !!</span></center>";
                        return $msg;
                    }
                }
            }else{
                $query = "update tbl_product set productName='$productName',catId='$catId',brandId='$brandId',quantity='$quantity',sellPrice='$price',status='$productStatus' where productId='$id'";
                $productUpdate = $this->db->update($query);
                if ($productUpdate) {
                    $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Product Updated Successfully!!</span></center>";
                    return $msg;
                } else {
                    $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Product not Updated !!</span></center>";
                    return $msg;
                }

            }

        }
    }

    public function delProductById($id){

        $query = "select * from tbl_product where productId = '$id'";
        $getDate = $this->db->select($query);
        if ($getDate){
            while ($delimage = mysqli_fetch_assoc($getDate)){
                $deletelink = $delimage['image'];
                unlink($deletelink);
            }
        }
        $delquery = "delete from tbl_product where productId = '$id'";
        $result = $this->db->delete($delquery);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Product Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Product not Deleted !!</span></center>";
            return $msg;
        }
    }

    public function getFeaturedProduct(){

        $query = "select * from tbl_product where type = '0' order by productId desc limit 4";
        $result = $this->db->select($query);

        return $result;
    }

    public function getNewProduct(){
        $query = "select * from tbl_product where type = '1' order by productId desc limit 4";
        $result = $this->db->select($query);

        return $result;

    }

    public function getSingleProduct($id){

        $query = "select tbl_product.*,tbl_category.catName,tbl_brands.brandName
                  from tbl_product
                  inner join tbl_category
                  on tbl_product.catId = tbl_category.catId
                  inner join tbl_brands
                  on tbl_product.brandId = tbl_brands.brandId
                  and tbl_product.productId = '$id'";
        $result = $this->db->select($query);
        return $result;

    }

    public function getLatestIphone(){

        $query = "select * from tbl_product where brandId = '1' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }

    public function getLatestSamsung(){

        $query = "select * from tbl_product where brandId = '4' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }
    public function getLatestAcer(){

        $query = "select * from tbl_product where brandId = '6' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }
    public function getLatestCanon(){

        $query = "select * from tbl_product where brandId = '7' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }

    public function productByCat($id){
        $catId = mysqli_real_escape_string($this->db->link, $id);

        $query = "select * from tbl_product where catId = '$catId'";
        $result = $this->db->select($query);

        return $result;
    }

    public function insertCompareData($customerId,$cmprId){
        $customerId = mysqli_real_escape_string($this->db->link,$customerId);
        $productId = mysqli_real_escape_string($this->db->link,$cmprId);
        $cheakCompare = "select * from tbl_compare where customerId = '$customerId' and productId = '$productId'";
        $cheak = $this->db->select($cheakCompare);
        if($cheak){
            $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Already Added !!</span></center>";
            return $msg;
        }

        $query = "select * from tbl_product where productId = '$productId'";
        $result = $this->db->select($query);
        $result = mysqli_fetch_assoc($result);
        if ($result){
                $productId = $result['productId'];
                $productName = $result['productName'];
                $price = $result['price'];
                $image = $result['image'];
                $query = "insert into tbl_compare(customerId,productId,productName,price,image) values('$customerId','$productId','$productName','$price','$image')";
                $compareInsert = $this->db->insert($query);
            if($compareInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;color:green;text-align:center;font-size: 15px'>Added !! Cheak Compare Page !!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Not Added !!</span></center>";
                return $msg;
            }

        }

    }

    public function getComparedProduct($customerId){
        $customerId = mysqli_real_escape_string($this->db->link, $customerId);

        $query = "select * from tbl_compare where customerId = '$customerId' order by id desc";
        $result = $this->db->select($query);

        return $result;
    }

    public function delCompareData($customerId){
        $customerId = mysqli_real_escape_string($this->db->link, $customerId);

        $query = "delete from tbl_compare where customerId = '$customerId'";
        $result = $this->db->delete($query);

    }

    public function getAllProductCount()
    {
        $query = "select * from tbl_product where status = 1 ";
        $result = $this->db->select($query);

        return $result;
    }

    public function productDataByStatus(){
        $productSql = "SELECT * FROM tbl_product WHERE  status = 1 AND quantity != 0";
        $productData = $this->db->select($productSql);
        return $productData;

    }

    public function orderInsert($data){


        $orderDate 						= date('Y-m-d', strtotime($data['orderDate']));
        $clientName 					= $data['clientName'];
        $clientContact 				= $data['clientContact'];
        $subTotalValue 				= $data['subTotalValue'];
        $vatValue 						=	$data['vatValue'];
        $totalAmountValue     = $data['totalAmountValue'];
        $discount 						= $data['discount'];
        $grandTotalValue 			= $data['grandTotalValue'];
        $paid 								= $data['paid'];
        $dueValue 						= $data['dueValue'];
        $paymentType 					= $data['paymentType'];
        $paymentStatus 				= $data['paymentStatus'];


        $sql = "INSERT INTO tbl_order (order_date, client_name, client_contact, sub_total, vat, total_amount, discount, grand_total, paid, due, payment_type, payment_status, order_status) VALUES ('$orderDate', '$clientName', '$clientContact', '$subTotalValue', '$vatValue', '$totalAmountValue', '$discount', '$grandTotalValue', '$paid', '$dueValue', $paymentType, $paymentStatus, 1)";


        $order_id ='';
        $orderStatus = false;
        if($this->db->insert($sql) === true) {
            $order_id = mysqli_insert_id($this->db->link);
            $valid['order_id'] = $order_id;
            $orderStatus = true;
            if($orderStatus = true){
                $msg = "Successfully Added";
                echo json_encode($msg);
                return $msg;
            }else{
                $msg = "Not Added";
                echo json_encode($msg);
                return $msg;
            }
        }


        // echo $_POST['productName'];
        $orderItemStatus = false;

        for($x = 0; $x < count($_POST['productName']); $x++) {
            $updateProductQuantitySql = "SELECT tbl_product.quantity FROM tbl_product WHERE tbl_product.productId = ".$_POST['productName'][$x]."";
            $updateProductQuantityData = $this->db->select($updateProductQuantitySql);


            while ($updateProductQuantityResult = mysqli_fetch_assoc($updateProductQuantityData)) {
                $updateQuantity[$x] = $updateProductQuantityResult[0] - $_POST['quantity'][$x];
                // update product table
                $updateProductTable = "UPDATE tbl_product SET quantity = '".$updateQuantity[$x]."' WHERE productId = ".$_POST['productName'][$x]."";
                $this->db->update($updateProductTable);

                // add into order_item
                $orderItemSql = "INSERT INTO order_item (order_id, product_id, quantity, rate, total, order_item_status) 
				VALUES ('$order_id', '".$_POST['productName'][$x]."', '".$_POST['quantity'][$x]."', '".$_POST['rateValue'][$x]."', '".$_POST['totalValue'][$x]."', 1)";

                $this->db->insert($orderItemSql);

                if($x == count($_POST['productName'])) {
                    $orderItemStatus = true;
                }
            } // while
        } // /for quantity
        if($orderItemStatus){
            $msg = "Successfully Added";
            echo json_encode($msg);
            return $msg;
        }else{
            $msg = "Not Added";
            echo json_encode($msg);
            return $msg;
        }


   //    echo json_encode($valid);

    }

}